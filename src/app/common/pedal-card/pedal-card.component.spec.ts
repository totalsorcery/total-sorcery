import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PedalCardComponent } from './pedal-card.component';

describe('PedalCardComponent', () => {
  let component: PedalCardComponent;
  let fixture: ComponentFixture<PedalCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PedalCardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PedalCardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
